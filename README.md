# Installing
## Rust
### Rustup
Rustup is an installer for Rust that handles updates and branch changes (stable/nightly)  
If you do not have Rustup, it can be installed from [here](https://rustup.rs/)  

Rust Nightly is required to work with WebAssembly and can be setup using:  
`rustup default nightly`  

We also need to the add the WebAssembly target:  
`rustup target add wasm32-unknown-unknown`  

We also need the Bindgen CLI tool to help generate WebAssembly code that can use Javascript types:  
`cargo +nightly install wasm-bindgen-cli`  

## Web App
Navigate to the `web` directory:  
```
npm install
npm run build
npm run serve
```  

Then navigate to `localhost:8080` in your browser
