#![feature(proc_macro, wasm_custom_section, wasm_import_module)]

extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn selection_sort(mut items: Vec<i32>) -> Vec<i32> {
    for i in 0..items.len() {
        let mut min = i;
        for j in i..items.len() {
            if items[j] < items[min] {
                min = j
            }
        }
        let tmp = items[i];
        items[i] = items[min];
        items[min] = tmp;
    }
    items
}
