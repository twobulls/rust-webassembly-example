const rust = import('./wasm/rust_webassembly_example')

rust.then(rust => {
  let length = 2000
  let randomArray = Array.from({length: length}, () => Math.floor(Math.random() * length));

  let rustStartTime = performance.now()
  rust.selection_sort(randomArray)
  getTimeDifference(rustStartTime, performance.now(), "Rust")

  let jsStartTime = performance.now()
  selectionSort(randomArray)
  getTimeDifference(jsStartTime, performance.now(), "JS")
});

function selectionSort(items) {
  for (i = 0; i < items.length; i++) {
    let min = i;
    for (j = i; j < items.length; j++) {
      if (items[j] < items[min]) {
        min = j
      }
    }
    let tmp = items[i];
    items[i] = items[min];
    items[min] = tmp;
  }
  return items
}

function getTimeDifference(start, end, from) {
  console.log(`${from}: ${end - start}ms (+-2ms)`)
}
